#include <cassert>
#include <cstdlib>
#include <cstring>
#include "akinator.h"

long fileSize( FILE* file )
{
    assert( nullptr != file );
    assert( ferror( file ) == 0 );

    long currentPosition = ftell( file );

    fseek( file, 0, SEEK_END );
    long size  = ftell( file );

    fseek( file, currentPosition, SEEK_SET );

    return size;
}

tree_t* get_akinator_data_tree( char filename[] )
{
    FILE* database = fopen( filename, "r" );
    long buf_size = fileSize(database);

    char* buf = (char*)calloc( sizeof( char ), buf_size + 1 );

    fread( buf, sizeof( char ), buf_size, database );

    int char_readen = 0;

    char* poison = POISON_NAME;

    tree_t* data = construct_tree( "DATA", sizeof( char* ), cstring_default_funcs, &poison );
    assert( data );
    node_t* root = read_akinator_database( data, buf, &char_readen, buf_size );
    assert( root );

    free( buf );

    set_root( data, root );

    fclose( database );

    return  data;
}

/*TODO*/
void print_akinator_node( FILE* fstream, node_t* node )
{
    assert( fstream );

    static int tabs = 0;

    if( node != nullptr )
    {
        for( int i = 0; i < tabs; ++i )
            fprintf( fstream, "\t" );

        fprintf( fstream, "(\"%s\"\n", *(char**)(node->data) );

        tabs++;

        print_akinator_node( fstream, node->left );
        print_akinator_node( fstream, node->right );

        tabs--;

        for( int i = 0; i < tabs; ++i )
            fprintf( fstream, "\t" );

        fprintf( fstream, ")\n" );
    }
}

void print_akinator_database( char filename[], tree_t* data )
{
    assert( filename );
    assert( data );

    FILE* fstream = fopen( filename, "w" );
    assert( fstream );

    fprintf( fstream, "\"\n" );

    print_akinator_node( fstream, data->root );

    fprintf( fstream, "\n" );

    fclose( fstream );
}

node_t*     read_akinator_database( tree_t* data_tree, char* buf, int* pos, long buf_size )
{
    assert( data_tree );
    assert( buf );
    assert( pos );

    assert( *pos < buf_size );

    static char sep = '\0';
    if( *pos == 0 )
    {
        SCAN_DATA_STR( buf, pos, "%c", &sep );
        assert( sep == '\'' || sep == '\"' );
    }

    char next_bracket = '\0';

    SCAN_DATA_STR( buf, pos, "%c", &next_bracket );
    assert( next_bracket == '(' || next_bracket == ')' );

    if( next_bracket == ')' )
        return nullptr;
    else if( next_bracket == '(' )
    {
        char* data = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
        assert( data );

        SCAN_DATA_STR( buf, pos, "%*[\'\"]%[^\'\"]%*[\'\"]", data );

        node_t* node = construct_node( data_tree, &data );
        free( data );

        //Костыль
        node_t* left    = read_akinator_database( data_tree, buf, pos, buf_size );
        node_t* right   = nullptr;
        if( left != nullptr )
        {
            right = read_akinator_database( data_tree, buf, pos, buf_size );
            assert( right );

            while( *pos < buf_size && *(buf + *pos + 1) != '(' ) *pos += 1;

            set_left( node, left );
            set_right( node, right );
            return node;
        }

        return node;
    }

    return nullptr;
}

game_result_t   akinator_game( FILE* ifstream, FILE* ofstream, tree_t* data_tree, node_t** end_node )
{
    assert( ifstream );
    assert( ofstream );
    assert( data_tree );

    node_t* node = data_tree->root;
    assert( node );

    while( node->left != nullptr && node->right != nullptr )
    {
        fprintf( ofstream, "Can your character be described as %s? (y/n): ", *(char**)(node->data) );

        char ans = '\0';

        SCAN_DATA( ifstream, "%c", &ans );

        if(      ans == 'y' ) { node = node->right; *end_node = node; }
        else if( ans == 'n' ) { node = node->left;  *end_node = node; }

        else return WRONG_INPUT;
    }

    fprintf( ofstream, "It's %s\nAm I right? (y/n): ", *(char**)node->data );

    char ans = '\0';
    SCAN_DATA( ifstream, "%c", &ans );

    if(      ans == 'y' ) return SUCCESS;
    else if( ans == 'n' ) return UNKNOWN_CHARACTER;

    return WRONG_INPUT;
}

int             add_character( FILE* ifstream, FILE* ofstream, node_t* parent )
{
    assert( ifstream );
    assert( ofstream );

    char* smth = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
    assert( smth );

    fprintf( ofstream, "Who is it?: " );

    SCAN_DATA( ifstream, "%[^\n]", smth );

    printf( "What differs %s from %s?: ", smth, *(char**)(parent->data) );
    char* diff = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
    assert( diff );

    SCAN_DATA( ifstream, "%[^\n]", diff );

    node_t* no   = construct_node( parent->_tree, &(*(char**)parent->data) );
    node_t* yes  = construct_node( parent->_tree, &smth );

    change_node_data( parent, &diff );
    set_left(  parent, no );
    set_right( parent, yes );

    free( smth );
    free( diff );

    return 0;
}

stack_t*        get_attributes( node_t* ch_node )
{
    assert( ch_node );

    char* poison = POISON_NAME;
    stack_t* result = construct_stack( *(char**)ch_node->data, sizeof( char* ), cstring_default_funcs,
                                       &poison, STK_DEFAULT_CAPACITY );

    assert( result );

    node_t* prev_attr   = ch_node;
    node_t* attr        = ch_node->parent;

    while( attr != nullptr )
    {
        if( attr->right == prev_attr )
            stk_push( result, (char**)attr->data );
        else
        {
            char* str_atr = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
            assert( str_atr );

            sprintf( str_atr, "not %s", *(char**)attr->data );

            stk_push( result, &str_atr );

            free( str_atr );
        }

        attr = attr->parent;
        prev_attr = prev_attr->parent;
    }

    return result;
}

int             print_attributes( FILE* ofstream, stack_t* attributes )
{
    assert( attributes );
    assert( ofstream );
    assert( stk_size( attributes ) > 0 );

    stack_t* attrs = copy_stack( attributes );
    assert( attrs );
    int res = 0;

    char* attribute = nullptr;
    stk_pop( attrs, &attribute );

    fprintf( ofstream, "%s", attribute );
    res++;

    while( stk_size( attrs ) > 0 )
    {
        stk_pop( attrs, &attribute );
        fprintf( ofstream, " and %s", attribute );
        res++;
    }

    destruct_stack( attrs );
    free( attribute );

    return res;
}

int             print_definition( FILE* ofstream, stack_t* attributes )
{
    assert( attributes );
    assert( ofstream );
    assert( stk_size( attributes ) > 0 );

    fprintf( ofstream, "Your charachter can be described as " );

    int res = print_attributes( ofstream, attributes );

    fprintf( ofstream, ".\n" );

    return res;
}

int             print_comparation( FILE* ofstream, stack_t* ch1_attributes, stack_t* ch2_attributes )
{
    assert( ofstream );
    assert( ch1_attributes );
    assert( ch2_attributes );
    assert( ch1_attributes != ch2_attributes );
    assert( stk_size( ch1_attributes ) > 0 && stk_size( ch2_attributes ) > 0 );

    if( strcmp( ch1_attributes->_name, ch2_attributes->_name ) == 0 )
    {
        fprintf( ofstream, "Compare character with himself? Lol\n" );
        int res = print_definition( stdout, ch1_attributes );
        return res;
    }

    stack_t* ch1_attrs = copy_stack( ch1_attributes );
    stack_t* ch2_attrs = copy_stack( ch2_attributes );

    dump_stack( STACK_DUMP_FILE, ch1_attrs );
    dump_stack( STACK_DUMP_FILE, ch2_attrs );

    int res = 0;

    char* str_ch1_attr = nullptr; cont_error_t err1 = stk_pop( ch1_attrs, &str_ch1_attr );
    char* str_ch2_attr = nullptr; cont_error_t err2 = stk_pop( ch2_attrs, &str_ch2_attr );


    bool has_common = ( strcmp( str_ch1_attr, str_ch2_attr ) == 0 );

    if( has_common )
    {
        fprintf( ofstream, "Both characters can be described as %s", str_ch1_attr );

        err1 = stk_pop( ch1_attrs, &str_ch1_attr );
        err2 = stk_pop( ch2_attrs, &str_ch2_attr );

        res++;
    }
    else
    {
        fprintf( ofstream, "Charachters has nothing in common" );
        stk_push( ch1_attrs, &str_ch1_attr );
        stk_push( ch2_attrs, &str_ch2_attr );
    }

    while( strcmp( str_ch1_attr, str_ch2_attr ) == 0 &&
            ( err1 == NO_ERROR && err2 == NO_ERROR) )
    {
        assert( str_ch1_attr );
        assert( str_ch2_attr );

        fprintf( ofstream, " and %s", str_ch1_attr );

        err1 = stk_pop( ch1_attrs, &str_ch1_attr );
        err2 = stk_pop( ch2_attrs, &str_ch2_attr );

        res++;
    }

    if( has_common )
    {
        stk_push( ch1_attrs, &str_ch1_attr );
        stk_push( ch2_attrs, &str_ch2_attr );
    }

    fprintf( ofstream, "\nBut:\n" );

    fprintf( ofstream, "%s is ", ch1_attributes->_name );
    res += print_attributes( ofstream, ch1_attrs );
    fprintf( ofstream, "\n" );

    fprintf( ofstream, "When %s is ", ch2_attributes->_name );
    res += print_attributes( ofstream, ch2_attrs );
    fprintf( ofstream, "\n" );

    destruct_stack( ch1_attrs );
    destruct_stack( ch2_attrs );

    free( str_ch1_attr );
    free( str_ch2_attr );

    return res;
}
