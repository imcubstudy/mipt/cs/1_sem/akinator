#include <cstdio>
#include "containers.h"

#define MAX_NAME_LEN 1000
#define POISON_NAME  "$POISON$"

#define SCAN_DATA_STR( str, pos, format, dest )                                 \
    do                                                                          \
    {                                                                           \
        int pos_delta = 0;                                                      \
        sscanf( (str) + *(pos), "%*[ \t\n]%n", &pos_delta );                    \
        *(pos) += pos_delta; pos_delta = 0;                                     \
        int sc_res = sscanf( (str) + *(pos), format"%n", (dest), &pos_delta );  \
        assert( sc_res != 0 );                                                  \
        *(pos) += pos_delta;                                                    \
    } while( 0 )

#define SCAN_DATA( fstream, format, dest )                                      \
    do                                                                          \
    {                                                                           \
        fscanf( fstream, "%*[ \t\n]" );                                         \
        int sc_res = fscanf( fstream, format, (dest) );                         \
        assert( sc_res != 0 );                                                  \
    } while( 0 )

using namespace vl_containers;
#define stack_t  vl_containers::stack_t

long        fileSize( FILE* file );

node_t*     read_akinator_database( tree_t* data_tree, char* buf, int* pos, long buf_size );
tree_t*     get_akinator_data_tree( char filename[] );

void        print_akinator_node( node_t* node, FILE* fstream );
void        print_akinator_database( char filename[], tree_t* data );

enum game_result_t
{
    SUCCESS,
    UNKNOWN_CHARACTER,
    WRONG_INPUT
};

game_result_t   akinator_game( FILE* ifstream, FILE* ofstream, tree_t* data_tree, node_t** end_node );
int             add_character( FILE* ifstream, FILE* ofstream, node_t* parent );

stack_t*        get_attributes( node_t* ch_node );

int             print_attributes( FILE* ofstream, stack_t* attributes );
int             print_definition( FILE* ofstream, stack_t* attributes );
int             print_comparation( FILE* ofstream, stack_t* ch1_attrs, stack_t* ch2_attrs );
