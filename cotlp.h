#define CONT_LOG$
#define FULL_CONT_LOG$

#define    MAX_TREE_NODE_DUMPS$    1024
#define    MAX_LIST_LINKS_DUMPS$    1024

#define    MAX_MSG_LEN$            1024

#define    LOG_FILE_NAME$            ".CONTAINERS_LOG"

#define    TREE_DUMP_FILE_NAME$    ".TREE_T_DUMP"
#define    STACK_DUMP_FILE_NAME$   ".STACK_T_DUMP"
#define    LIST_DUMP_FILE_NAME$    ".LIST_T_DUMP"

