#include <cstdio>
#include <string>
#include <cassert>
#include "akinator.h"

int main( int argc, char* argv[] )
{
    assert( argc == 3 );

    tree_t* data = get_akinator_data_tree( argv[1] );

    printf( "Input mode( (p)lay, (c)ompare, (d)efine ): " );
    char mode = '\0';
    SCAN_DATA( stdin, "%c", &mode );

    switch( mode )
    {
        case 'p':
        {
            node_t* end = nullptr;
            game_result_t game_result = akinator_game( stdin, stdout, data, &end );

            switch( game_result )
            {
                case SUCCESS:
                {
                    printf( "Just like i said!\n" );
                    break;
                }
                case UNKNOWN_CHARACTER:
                {
                    add_character( stdin, stdout, end );
                    break;
                }
                case WRONG_INPUT:
                {
                    printf( "You have to use y/n\n" );
                    break;
                }
                default:
                {
                    assert( !"Unexpected game result" );
                }
            }
            break;
        }
        case 'd':
        {
            printf( "Input character's name: " );
            char* character = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
            SCAN_DATA( stdin, "%[^\n]", character );

            node_t* ch_node =find_leave_by_value( data->root, &character );
            free( character );
            if( ch_node == nullptr )
            {
                printf( "This character isn't in database \'%s\'\n", argv[1] );
                break;
            }

            stack_t* attributes = get_attributes( ch_node );

            print_definition( stdout, attributes );

            destruct_stack( attributes );

            break;
        }
        case 'c':
        {
            printf( "Compare ch(1) with ch(2)\n" );

            char* ch1_name = (char*)calloc( sizeof( char ), MAX_NAME_LEN );
            char* ch2_name = (char*)calloc( sizeof( char ), MAX_NAME_LEN );

            printf( "Input ch(1) name: " );
            SCAN_DATA( stdin, "%[^\n]", ch1_name );

            printf( "Input ch(2) name: " );
            SCAN_DATA( stdin, "%[^\n]", ch2_name );

            node_t* ch1_node = find_leave_by_value( data->root, &ch1_name ); free( ch1_name );
            node_t* ch2_node = find_leave_by_value( data->root, &ch2_name ); free( ch2_name );
            if( ch1_node == nullptr || ch2_node == nullptr )
            {
                printf( "Can't compare them, I dunno them\n" );
                break;
            }

            stack_t* ch1_attrs = get_attributes( ch1_node );
            stack_t* ch2_attrs = get_attributes( ch2_node );

            print_comparation( stdout, ch1_attrs, ch2_attrs );

            destruct_stack( ch1_attrs );
            destruct_stack( ch2_attrs );

            break;
        }
        default:
        {
            printf( "What do you want from me?\n" );
        }
    }

    dot_tree( TREE_DUMP_FILE, data );

    print_akinator_database( argv[2], data );

    destruct_tree( data );

    return 0;
}

